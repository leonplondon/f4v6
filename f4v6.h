/*-----------------------------------------------------------
    Program: f4 version 6
    By: Daniel Cabarcas. cabarcas@gmail.com
    Part of: Master thesis "An Implementation of Faug�re's F4
             Algorithm for Computing Gr\"obner Bases".
-------------------------------------------------------------*/
/** \file f4v6.h
    Declaration of functions and structures of the f4 algorithm.    
*/
#ifndef F4V6_H
#define F4V6_H

#include<set>
#include<vector>
#include<list>
using namespace std;
#include "list_poly.h"

#define MAX_DEG  20               ///< maximun degree of computation
#define MAX_STEP 50               ///< maximum number of steps allowed in the F4 algorithm
/// Implements a pair of polynomials
struct Poly_poly
{
    int i_f1, i_f2;         ///< Pointers to polynomials in GG[step1] and GG[step2]
    short step1, step2;     ///< Pointers to List_poly in GG
    /// Constructor
    Poly_poly(const short s1, const int i1, const short s2, const int i2)
    {
        step1 = s1; step2 = s2; i_f1 = i1; i_f2 = i2;
    }
};
/// Implements a Monomial-Polynomial pair
struct Signature
{
    Monomial::Index t;  ///< Monomial
    short step;         ///< Pointer to List_poly in GG
    int i_f;            ///< Pointer to polynomial in GG[step]
    Monomial::Index lt; ///< leading monomial of t*f
    /// Constructor
    Signature(Monomial::Index t1 = -1, short s1 = -1, int i_f1 = -1, Monomial::Index lt1 = -1)
    {
        t = t1;
        step = s1;
        i_f = i_f1;
        lt = lt1;
    }
};
/// == operator for Signatures
inline bool operator==(const Signature & s1, const Signature & s2)
{
    return  s1.i_f  == s2.i_f   &&
            s1.lt   == s2.lt    &&
            s1.step == s2.step  &&
            s1.t    == s2.t     ;
}
//------------ Function declaration
short select(vector<list<Poly_poly>*> &P);
void left_right(const list<Poly_poly> &Bstar,
                const vector<List_poly> &GG,
                list<Signature> *L);
void symbolic_preprocessing(const vector<List_poly> &GG,
                            const vector<vector<bool> > &ncsry,
                            const vector<list<Signature> > &HH,
                            List_poly *H,
                            list<Signature> *L);
void simplify(const vector<List_poly> &GG,
              const vector<list<Signature> > &HH,
              Signature *sig);
void remove_if_LM_in(const List_poly &G,
                     vector<bool> *ncsry,
                     const set<Monomial::Index> &monomials);
void update(const vector<List_poly> &GG,
            vector<list<Poly_poly>*> *BB,
            vector<vector<bool> > *ncsry);

#endif
