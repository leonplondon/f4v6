/*-----------------------------------------------------------
    Program: f4 version 6
    By: Daniel Cabarcas. cabarcas@gmail.com
    Part of: Master thesis "An Implementation of Faug�re's F4
             Algorithm for Computing Gr\"obner Bases".
-------------------------------------------------------------*/
#include <ctime>
#include<string>
#include "list_poly.h"
#include "output.h"

using namespace std;

#ifdef PRINT
	ofstream fout;
#endif

void F4(const List_poly &F, List_poly &G);
void F4_test(const List_poly &F, List_poly &G);
//void sparse_test1();

/// Arguments: inputfilename outputfilename, numberofvars
int main(int argc, char* argv[])
{
    string strFin = "in.txt";
	string strFout = "out.txt";
    int n_var = 8;
	
	if(argc >= 2)
		strFin = argv[1];
	if(argc >= 3)
		strFout = argv[2];
    if(argc >= 4)
        n_var = atoi(argv[3]);
    
    Monomial::set_num_var( n_var );

#ifdef PRINT
    fout.open(strFout.data(), ios::out | ios::app);
    fout<<endl<<"F4v5. Reading file: "<<strFin;
#endif
#ifdef PRINT_DEBUG
    fout<<endl;
    monomial_test1();
#endif
    srand(5468);
    init();
    //Initialize
    Monomial::increase_indexed(2);
    List_poly F(2), G;

    //Generate random and update leading monomials
    //Monomial::set_num_var(3);
    //gen_rdm_list(F,3);
    read_magma(strFin.data(), F);
    update_lead_monomial(&F);
#ifdef PRINT_DEBUG
    fout<<endl<<"Original F:"<<endl<<F;
#endif
#if defined(PRINT_TIME) || defined(PRINT_PERFORMANCE)
    clock_t timer1 = clock();
#endif
    F4_test(F, G);
#if defined(PRINT_PERFORMANCE)
    fout<<endl;
#endif
#if defined(PRINT_TIME) || defined(PRINT_PERFORMANCE)
    fout<<'\t'<<(double)(clock() - timer1) / CLOCKS_PER_SEC;
#endif
#if defined(PRINT_MEMORY) || defined(PRINT_PERFORMANCE)
    PROCESS_MEMORY_COUNTERS pmc;
    if ( GetProcessMemoryInfo( GetCurrentProcess(), &pmc, sizeof(pmc)) )
        fout<<'\t'<< setprecision( 4 )<<(double)pmc.PeakWorkingSetSize / 1000000;
#endif
//    cout<<endl<<"Final G:"<<endl<<G<<endl;
#ifdef PRINT_DEBUG
    fout<<endl<<"Final G:"<<endl<<G<<endl;
#endif
#ifdef PRINT
    fout.close();
#endif	
//    if(!_CrtCheckMemory())
//        cout<<"Memory error";
}
